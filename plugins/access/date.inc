<?php

/**
 * @file
 * Plugin to provide access control based on a date.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Date"),
  'description' => t('Control access by checking a date.'),
  'callback' => 'ctools_date_access_date_access_check',
  'settings form' => 'ctools_date_access_date_access_settings',
  'summary' => 'ctools_date_access_date_access_summary',
);

/**
 * Settings form for the 'date' access plugin.
 */
function ctools_date_access_date_access_settings($form, &$form_state, $conf) {
  $form['settings']['operator'] = array(
    '#type' => 'select',
    '#options' => ctools_date_access_operators(),
    '#title' => t('Operator'),
    '#default_value' => $conf['operator'],
  );
  $form['settings']['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#default_value' => $conf['date'],
    '#description' => t('The date to check.'),
  );
  return $form;
}

/**
 * Check for access.
 */
function ctools_date_access_date_access_check($conf, $context) {
  $now = strtotime('midnight', date('U'));
  $date = ctools_date_access_timestamp($conf['date']);
  switch($conf['operator']) {
    case '<':
      return $now < $date;
      break;
    case '<=':
      return $now <= $date;
      break;
    case '=':
      return $now == $date;
      break;
    case '!=':
      return $now != $date;
      break;
    case '>=':
      return $now >= $date;
      break;
    case '>':
      return $now > $date;
      break;
  }
  return;
}

/**
 * Provide a summary description based upon the configured date.
 */
function ctools_date_access_date_access_summary($conf, $context) {
  $timestamp = ctools_date_access_timestamp($conf['date']);
  $operators = ctools_date_access_operators();
  return t('Current date is @operator @date', array('@date' => format_date($timestamp, 'short'), '@operator' => $operators[$conf['operator']]));
}

/**
 * Operators to compare dates.
 */
function ctools_date_access_operators() {
  return array(
    '<' => 'Is less than',
    '<=' => 'Is less than or equal to',
    '=' => 'Is equal to',
    '!=' => 'Is not equal to',
    '>=' => 'Is greater than or equal to',
    '>' => 'Is greater than',
  );
}

/**
 * Return a timestamp from the form api date field.
 * @param $date
 *    Array of date information.
 */
function ctools_date_access_timestamp($date) {
  $date = $date['year'] . '-' . $date['month'] . '-' . $date['day'];
  return strtotime($date);
}
